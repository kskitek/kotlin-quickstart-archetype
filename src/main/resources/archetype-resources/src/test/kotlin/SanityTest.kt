import io.kotlintest.matchers.shouldEqual
import io.kotlintest.specs.StringSpec

class SanityTest : StringSpec() {
    init {
        "Test passes" {
            1 shouldEqual 1
        }

        "Test does not pass" {
            1 shouldEqual 2
        }
    }
}